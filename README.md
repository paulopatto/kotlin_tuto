# Kotlin tutorial

Exemplo de aplicação com o Kotlin.



----
#### Referencias

- [Como usar o Kotlin no android studio](https://blog.jetbrains.com/kotlin/2013/08/working-with-kotlin-in-android-studio/).
- [Getting started with Android and Kotlin](https://kotlinlang.org/docs/tutorials/kotlin-android.html).
- [Beginning with Kotlin on Android (very basic)](https://youtu.be/e6sG9Qb-aCU).
