package com.example.paulopatto.kotlin_tuto

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class Main_Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_)
    }

    override fun onCreateOptionsMenu( menu: Menu ): Boolean {
        getMenuInflater().inflate( R.menu.menu_main, menu )
        return true
    }
}
